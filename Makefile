ASM = nasm 
ASMFLAG = -f elf64
LD = ld
PY = python3

OBJS = lib.o dict.o main.o

program: $(OBJS)
	$(LD) -o $@ $^

%.o: %.asm
	${ASM} ${ASMFLAG} -o $@ $^

test:
	$(PY) test.py

clean:
	rm -f *.o program

sum: clean program test

.PHONY: test clean
ssssssssssssssssssssssssssssss
