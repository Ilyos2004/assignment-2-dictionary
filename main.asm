%include "words.inc"
%include "lib.inc"

%define buff_size 256
%define psize 8

section .rodata
not_found: db "Key not found.", 0
key_too_long: db "Key is too long (>255).", 0

section .bss
buffer resb 256

section .text

global _start

extern find_word

_start:
    mov rdi, buffer
    mov rsi, buff_size
    call read_word

    test rax, rax
    jz .key_too_long

    mov rdi, rax
    mov rsi, elem
    call find_word

    test rax, rax
    jz .key_not_found

    mov rdi, rax
    add rdi, psize
    push rdi
    call string_length

    pop rdi
    lea rdi, [rdi+rax+1]
    call print_string

    jmp .exit

    .key_too_long:
        mov rdi, key_too_long
        jmp .print_err
    
    .key_not_found:
        mov rdi, not_found

    .print_err:
        call print_error
        jmp .exit

