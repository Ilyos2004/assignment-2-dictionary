section .text

%define SYS_EXIT 60
%define SYS_WRITE 1
%define SYS_READ  0
%define STDOUT 1
%define STDIN  0
%define STDERR 2


global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error


exit:
    mov rax, SYS_EXIT
    syscall


string_length:
    mov  rax, rdi
  .counter:
    cmp  byte [rdi], 0
    je   .end
    inc  rdi
    jmp  .counter
  .end:
    sub  rdi, rax
    mov  rax, rdi
    ret


print_string:
    push rdi
    call string_length
    mov  rdx, rax
    pop rsi
    mov  rax, SYS_WRITE
    mov  rdi, STDOUT
    syscall
    ret


print_char:
    push rdi
    mov rsi, rsp
    mov rax, SYS_WRITE
    mov rdi, STDIN
    mov rdx, 1
    syscall
    pop rdi
    ret



print_newline:
    mov rdi, 10
    jmp print_char



print_uint:
    xor rax, rax
    xor rdx, rdx
    xor rsi, rsi

    push rbx
    mov rbx, rsp
    mov rax, rdi
    mov rsi, 10
    mov [rsp], byte 0

    .loop:
        div rsi

        lea rdx, ['0' + rdx]
        dec rsp
        mov [rsp], dl
        xor rdx, rdx
        test rax, rax
        jnz .loop

    mov rdi, rsp
    call print_string

    mov rsp, rbx
    pop rbx
    ret


print_int:
    test rdi, rdi
    jns .print_uint
    sub rsp, 8
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    add rsp, 8
    neg rdi

    .print_uint:
        jmp print_uint


string_equals:
    xor rax, rax
    xor rcx, rcx

    .loop:
        mov al, byte [rdi + rcx]
        cmp al, byte [rsi + rcx]
        jne .ne
        cmp byte [rdi + rcx], 0
        je .eq
        cmp byte [rsi + rcx], 0
        je .eq
        inc rcx
        jmp .loop

    .ne:
        mov rax, 0
        ret

    .eq:
        mov rax, 1
        ret


read_char:
    sub rsp, 32      ; Allocate space for a character
    mov rax, SYS_READ
    mov rdi, STDIN
    mov rsi, rsp
    mov rdx, 1
    syscall
    cmp rax, 0
    jle .eof
    movzx rax, byte [rsp]
    add rsp, 32      ; Restore the stack
    ret

.eof:
    add rsp, 32      ; Restore the stack
    test rax, rax
    ret

read_word:
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    xor r14, r14

.loop:
    call read_char
    test rax, rax
    jz .done
    cmp r14, r13
    jnl .err
    cmp rax, 0x20
    jz .skip
    cmp rax, 0x9
    jz .skip
    cmp rax, 0xA
    jz .skip
    mov [r12 + r14], al
    inc r14
    jmp .loop

.skip:
    test r14, r14
    jz .loop

.done:
    mov byte[r12 + r14], 0
    mov rax, r12
    mov rdx, r14
    jmp .exit

.err:
    xor rax, rax

.exit:
    pop r14
    pop r13
    pop r12
    ret


parse_uint:
    xor rax, rax
    xor rdx, rdx

    .read_loop:
        movzx rcx, byte [rdi + rdx]

        test rcx, rcx
        jz .done

        cmp rcx, '0'
        jb .not_a_digit
        cmp rcx, '9'
        ja .not_a_digit

        sub rcx, '0'

        imul rax, rax, 10
        add  rax, rcx

        inc  rdx
        jmp  .read_loop

    .not_a_digit:
        test rax, rax
        jnz .done
        xor rdx, rdx

    .done:
        ret


parse_int:
    xor rsi, rsi
    cmp byte[rdi], '-'
    je .negative
    cmp byte[rdi], '+'
    je .positive

    .unsigned:
        jmp parse_uint

    .negative:
        inc rdi
        call parse_uint

    inc rdx
    neg rax
    ret

    .positive:
        inc rdi

    jmp parse_uint


string_copy:
    xor rax, rax
    .loop:
            cmp rax, rdx
            jz .return_zero
            mov r9b, [rdi + rax]
            mov [rsi + rax], r9b
            inc rax
            test r9b, r9b
            jz .return
            jmp .loop

    .return_zero:
            xor rax, rax

    .return:
            ret

print_error:
    push rdi
    call string_length
    mov  rdx, rax
    pop  rsi
    mov  rax, SYS_WRITE
    mov  rdi, STDERR
    syscall
    call print_newline
    ret

